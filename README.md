# Rafalis server

## Description
The Rafalis server interfaces the database and the web client(s). It helps to process the uploaded images (ex thumbnail generation), and implements a login mechanism to provide different privilege levels in the client.

This project uses Quarkus, the Supersonic Subatomic Java Framework (https://quarkus.io/).

## Requirements

 - Java JDK 11
 - `JAVA_HOME` specified like this : `export JAVA_HOME=/path/to/jdk-11` 
 - `openssl` or any equivalent to generate a private/public rsa key pair.

## Running the server

### 1 - Setup

Since this server implements java web tokens (jwt) authentications, you need **a pair of private/public rsa keys**. I :

```bash
# generate a private key (you can leave the required fields blank)
$ openssl req -newkey rsa:2048 -new -nodes -keyout src/main/resources/privkey.pem -out csr.pem


# generate the associated public key
$ openssl rsa -in src/main/resources/privkey.pem -pubout > src/main/resources/pubkey.pem
```

Make sure they are placed in the `src/main/resources/` directory and specify them like this :
`src/main/resources/application.properties`
```
    [...]
22  mp.jwt.verify.publickey.location = pubkey.pem
23  smallrye.jwt.sign.key.location = privkey.pem
    [...]
```

### 2 - Run in development mode

1. Start the database

    If you cloned the [main rafalis](https://gitlab.com/rafalis/rafalis) repository recursively, run the following :

    ```bash
    cd ../rafalis-db && docker-compose up -d
    ```

2. Start the server

    Assuming your `JAVA_HOME` is configured properly and you have generated a public/private rsa key pair (see [requirements](#requirements) above for help), you can run your application in dev mode using :
    ```shell script
    ./mvnw compile quarkus:dev
    ```

    > **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

3. (run only once) Post start script

    Run this step only if that was the first time you launched the rafalis database and the rafalis server.
    
    By default, the database is initialized with `varchar(255)` types for string fields (initialized by hibernate). This is not wanted, it limits fields length to 255 characters, which is not suited to write down long texts (like in collections). A post-install script is furnished in the [rafalis database repository](https://gitlab.com/rafalis/rafalis-db) :
    
    ```bash
    cd ../rafalis-db && ./post-start-install.sh
    ```

    This will set the right column types in the database (output similar to "`Columns properly formatted !`").


### 3 - Package and run production mode

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

### (optional) Creating a native executable

You can create a native executable instead of a jvm one using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/rafalis-1.0.0-SNAPSHOT-runner`
