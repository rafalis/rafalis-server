CREATE TABLE IF NOT EXISTS APPUSER
(
    id        BIGINT NOT NULL
        PRIMARY KEY,
    firstname TEXT,
    lastname  TEXT,
    email     TEXT,
    admin     BOOLEAN,
    enabled   BOOLEAN,
    password  TEXT,
    salt      TEXT
);

CREATE TABLE IF NOT EXISTS IMAGES
(
    id                     BIGINT NOT NULL PRIMARY KEY,
    name                   TEXT unique,
    title                  TEXT,
    show_in_browse         BOOl,
    description            TEXT,
    datetime               TEXT,
    author                 TEXT,
    camera                 TEXT,
    tags                   TEXT,
    isPublic               BOOL default true,
    showInHome            BOOL,
    collections_appearance TEXT,
    map_appearance         bigint,
    image                  bytea  NOT NULL,
    image_small            bytea  NOT NULL,
    thumb                  bytea  NOT NULL,
    mini_thumb             bytea  NOT NULL,
    palette                text, -- hex value for each color,
    small_width INT,
    small_height INT,
    thumb_width INT,
    thumb_height INT,
    minithumb_width INT,
    minithumb_height INT
);

CREATE TABLE IF NOT EXISTS COLLECTIONS
(
    id              BIGINT NOT NULL PRIMARY KEY,
    name            TEXT unique,
    description     TEXT,
    thumbnailID     BIGINT,
    structure       TEXT,
    imageContent    TEXT,
    textContent     TEXT,
    ispublic        BOOL default true,
    map_id          BIGINT,
    carouselcontent TEXT,
    lastModified    BIGINT
);

CREATE TABLE IF NOT EXISTS MAPS
(
    id                    BIGINT NOT NULL PRIMARY KEY,
    name                  TEXT,
    filename              TEXT UNIQUE,
    geojson               BYTEA  NOT NULL,
    referenced_images     TEXT,
    referenced_collection BIGINT
);

CREATE TABLE IF NOT EXISTS APPCONFIG
(
    id         BIGINT NOT NULL PRIMARY KEY,
    authors    TEXT default '-',
    cameras    TEXT default '-',
    main_title TEXT default 'My Gallery',
    sub_title  TEXT default '-'
);

CREATE FUNCTION collection_map() RETURNS trigger AS
$collection_map$
BEGIN
    UPDATE COLLECTIONS c
    SET map_id = NEW.id
    WHERE c.id = NEW.referenced_collection;
    RETURN null;
END;
$collection_map$ LANGUAGE plpgsql;

CREATE TRIGGER collection_map
    AFTER INSERT OR UPDATE
    ON MAPS
    FOR EACH ROW
    WHEN ( NEW.referenced_collection is not null )
EXECUTE FUNCTION collection_map();
