package ch.picportos.users.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "APPUSER")
public class UserDB {
    @Id
    @GeneratedValue
    private Long id;

    private String firstname;
    private String lastname;
    private boolean admin;
    private String email;
    private boolean enabled;
    private String password;
    private String salt;
}
