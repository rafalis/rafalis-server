package ch.picportos.users;

import ch.picportos.users.persistence.UserDB;
import org.apache.shiro.crypto.hash.Sha512Hash;

import java.util.UUID;

public class PasswordUtils {
    private static final String HASH_KEY = "2ab872a6-87e7-4ab3-9159-b6aa1689ab1f";
    private static final int HASH_ITERATION = 2;

    public static void setPassword(UserDB userDB, String clearPassword) {
        userDB.setSalt(UUID.randomUUID().toString());
        userDB.setPassword(
                new Sha512Hash(clearPassword, HASH_KEY + userDB.getSalt(), HASH_ITERATION)
                        .toBase64()
        );
    }

    public static boolean checkPassword(UserDB userDB, String clearPassword) {
        String hashedPassword = new Sha512Hash(clearPassword, HASH_KEY + userDB.getSalt(), HASH_ITERATION).toBase64();
        return hashedPassword.equals(userDB.getPassword());
    }
}
