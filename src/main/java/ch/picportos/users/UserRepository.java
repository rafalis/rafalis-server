package ch.picportos.users;

import ch.picportos.users.persistence.UserDB;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.Optional;

@ApplicationScoped
@JBossLog
public class UserRepository implements PanacheRepository<UserDB> {

    public Optional<UserDB> findByEmail(String email) {
        return find("email", email).singleResultOptional();
    }

    @Transactional
    public void removeCurrentAdmin() {
        Optional<UserDB> currentAdmin = find("admin", true).singleResultOptional();
        if (currentAdmin.isPresent()) {
            log.warn("Removing current creator : " + currentAdmin.get().getEmail());
            delete(currentAdmin.get());
        }
    }

    @Transactional
    public void removeCurrentVIP() {
        Optional<UserDB> currentVIP = find("admin", false).singleResultOptional();
        if (currentVIP.isPresent()) {
            log.warn("Removing current viewer : " + currentVIP.get().getEmail());
            delete(currentVIP.get());
        }
    }
}
