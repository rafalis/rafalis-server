package ch.picportos.users;

import ch.picportos.users.persistence.UserDB;
import io.quarkus.runtime.StartupEvent;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
@JBossLog
public class DefaultUserCreator {
    @Inject
    UserRepository userRepository;

    @Inject
    UsersConfig usersConfig;

    void onStart(@Observes StartupEvent startupEvent) {
        if (userRepository.findByEmail(usersConfig.defaultAdminEmail()).isEmpty()) {
            userRepository.removeCurrentAdmin();
            createDefaultAdmin();
        }
        if (userRepository.findByEmail(usersConfig.defaultVipEmail()).isEmpty()) {
            userRepository.removeCurrentVIP();
            createDefaultVIP();
        }
    }

    @Transactional
    public void createDefaultAdmin() {
        UserDB userDB = new UserDB();
        userDB.setEmail(usersConfig.defaultAdminEmail());
        userDB.setFirstname("Picportos");
        userDB.setLastname("Admin");
        userDB.setEnabled(true);
        userDB.setAdmin(true);
        PasswordUtils.setPassword(userDB, usersConfig.defaultAdminPassword());
        userRepository.persist(userDB);
        log.info("Created the creator : " + userDB.getEmail());
    }

    @Transactional
    public void createDefaultVIP() {
        UserDB userDB = new UserDB();
        userDB.setEmail(usersConfig.defaultVipEmail());
        userDB.setFirstname("Picportos");
        userDB.setLastname("Vip");
        userDB.setEnabled(true);
        userDB.setAdmin(false);
        PasswordUtils.setPassword(userDB, usersConfig.defaultVipPassword());
        userRepository.persist(userDB);
        log.info("Created the viewer account : " + userDB.getEmail());
    }
}
