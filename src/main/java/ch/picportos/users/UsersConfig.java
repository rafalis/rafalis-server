package ch.picportos.users;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "picportos.users")
public interface UsersConfig {
    String defaultAdminEmail();
    String defaultAdminPassword();
    String defaultVipEmail();
    String defaultVipPassword();
}
