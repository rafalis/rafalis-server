package ch.picportos.auth;

import ch.picportos.auth.model.AuthRequest;
import ch.picportos.auth.model.AuthResponse;
import ch.picportos.common.errors.ForbiddenException;
import ch.picportos.users.PasswordUtils;
import ch.picportos.users.UserRepository;
import ch.picportos.users.persistence.UserDB;
import io.quarkus.security.identity.SecurityIdentity;
import lombok.extern.jbosslog.JBossLog;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/auth")
@Tag(name = "Authentication")
@RequestScoped
@JBossLog
public class Authentication {
    @Inject
    SecurityIdentity securityIdentity;

    @Inject
    UserRepository userRepository;

    @Inject
    TokenGenerator tokenGenerator;

    @Inject
    JsonWebToken jsonWebToken;

    private UserDB user;

    @PostConstruct
    void init() {
        if (!securityIdentity.isAnonymous()) {
            user = Optional.ofNullable(jsonWebToken)
                    .filter(token -> "access".equals(token.getClaim("token_type")))
                    .map(JsonWebToken::getSubject)
                    .map(Long::parseLong)
                    .map(userRepository::findById)
                    .filter(UserDB::isEnabled)
                    .orElse(null);
        }
    }

    public void ensureIsAdmin() {
        if (!user().isAdmin()) {
            throw new ForbiddenException();
        }
    }

    public UserDB user() {
        ensureIsAuthenticated();
        return user;
    }

    public void ensureIsAuthenticated() {
        if (!isAuthenticated()) {
            throw new ForbiddenException();
        }
    }

    public boolean isAuthenticated() {
        return user != null;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AuthResponse auth(AuthRequest authRequest) {
        return userRepository.findByEmail(authRequest.getUsername())
                .filter(UserDB::isEnabled)
                .filter(user -> PasswordUtils.checkPassword(user, authRequest.getPassword()))
                .map(tokenGenerator::generateToken)
                .map(AuthResponse::new)
                .orElseThrow(() -> new ForbiddenException("Could not authenticate you"));
    }
}
