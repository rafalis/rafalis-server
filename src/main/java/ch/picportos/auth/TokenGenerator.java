package ch.picportos.auth;

import ch.picportos.users.persistence.UserDB;
import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.jwt.Claims;

import javax.enterprise.context.Dependent;
import java.util.StringJoiner;

@Dependent
public class TokenGenerator {
    public String generateToken(UserDB userDB) {
        return jwtClaims(userDB)
                .claim("token_type", "access")
                .claim("admin", userDB.isAdmin())
                .claim(Claims.auth_time.name(), System.currentTimeMillis() / 1000L)
                .expiresAt(System.currentTimeMillis() / 1000L + 3600L * (userDB.isAdmin() ? 24 : 72))
                .sign();
    }

    private JwtClaimsBuilder jwtClaims(UserDB userDB) {
        return Jwt.claims()
                .issuer("Picportos")
                .subject("" + userDB.getId())
                .claim(Claims.email.name(), userDB.getEmail())
                .claim(Claims.full_name.name(), fullName(userDB))
                .issuedAt(System.currentTimeMillis() / 1000L);
    }

    String fullName(UserDB userDB) {
        var joiner = new StringJoiner(" ");
        if (StringUtils.isNotBlank(userDB.getFirstname())) {
            joiner.add(userDB.getFirstname());
        }
        if (StringUtils.isNotBlank(userDB.getLastname())) {
            joiner.add(userDB.getLastname());
        }
        return joiner.toString();
    }
}
