package ch.picportos.common.converters;

import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@JBossLog
public class LongListListConverter implements AttributeConverter<List<List<Long>>, String> {
    private static final String SPLIT_CHAR_LONG = ";";
    private static final String SPLIT_CHAR_LIST = "-";

    @Override
    public String convertToDatabaseColumn(List<List<Long>> longList) {
        if (longList == null || longList.size() == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < longList.size(); i++) {
            if (longList.get(i).size() > 0) {
                sb.append(StringUtils.join(longList.get(i), SPLIT_CHAR_LONG));
                if (i != longList.size() - 1) {
                    sb.append(SPLIT_CHAR_LIST);
                }
            }
        }
        return sb.toString();
    }

    @Override
    public List<List<Long>> convertToEntityAttribute(String string) {
        if (string == null || string.isEmpty()) {
            return new ArrayList<>();
        }
        ArrayList<List<Long>> arrayLists = new ArrayList<>();
        for (String longsString : string.split(SPLIT_CHAR_LIST)) {
            if (!longsString.isEmpty()) {
                List<Long> longs = Arrays.stream(longsString.split(SPLIT_CHAR_LONG))
                        .map(Long::parseLong)
                        .collect(Collectors.toList());
                arrayLists.add(longs);
            }
        }
        return arrayLists;
    }
}
