package ch.picportos.common.converters;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.*;
import java.util.stream.Collectors;

@Converter
public class LongListConverter implements AttributeConverter<List<Long>, String> {
    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(List<Long> longList) {
        if (longList == null || longList.size() == 0) {
            return null;
        }
        return StringUtils.join(longList, SPLIT_CHAR);
    }

    @Override
    public List<Long> convertToEntityAttribute(String string) {
        if (string == null || string.isEmpty()) {
            return new ArrayList<>();
        }
        return Arrays.stream(string.split(SPLIT_CHAR)).map(Long::parseLong).collect(Collectors.toList());
    }
}
