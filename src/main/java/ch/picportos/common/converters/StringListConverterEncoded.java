package ch.picportos.common.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Converter
public class StringListConverterEncoded implements AttributeConverter<List<String>, String> {
    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(List<String> stringList) {
        if (stringList == null || stringList.size() < 1) {
            return null;
        }
        List<String> encodedStrings = stringList.stream()
                .map(val -> Base64.getEncoder().encode(val.getBytes(StandardCharsets.UTF_8)))
                .map(String::new)
                .collect(Collectors.toList());
        return String.join(SPLIT_CHAR, encodedStrings);
    }

    @Override
    public List<String> convertToEntityAttribute(String string) {
        if (string == null) {
            return emptyList();
        }
        return Arrays.stream(string.split(SPLIT_CHAR))
                .map(val -> Base64.getDecoder().decode(val.getBytes(StandardCharsets.UTF_8)))
                .map(String::new)
                .collect(Collectors.toList());
    }
}