package ch.picportos.common.errors;

import javax.ws.rs.core.Response;

public class ForbiddenException extends ApiException {

    public ForbiddenException(String message) {
        super(Response.Status.FORBIDDEN, message);
    }

    public ForbiddenException() {
        this("Forbidden");
    }

}

