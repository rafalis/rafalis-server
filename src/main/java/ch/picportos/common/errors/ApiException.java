package ch.picportos.common.errors;

import org.jboss.resteasy.specimpl.ResponseBuilderImpl;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ApiException extends WebApplicationException {

    public ApiException(Response.Status status, String message) {
        super(new ResponseBuilderImpl()
                .status(status)
                .entity(new ErrorResponse(message))
                .build());
    }

    public ApiException(Response.Status status) {
        this(status, null);
    }

}
