package ch.picportos.common.errors;

import javax.ws.rs.core.Response;

public class NotFoundException extends ApiException {

    public NotFoundException(String message) {
        super(Response.Status.NOT_FOUND, message);
    }

    public NotFoundException() {
        this("Not found");
    }

}
