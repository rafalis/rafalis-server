package ch.picportos.common.multipart;

import ch.picportos.img.api.model.ImageDescription;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

public class MultipartBodyImage {

    @FormParam("image")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    public InputStream file;

    @FormParam("name")
    @PartType(MediaType.TEXT_PLAIN)
    public String name;

    @FormParam("description")
    @PartType(MediaType.TEXT_PLAIN)
    public String imageDescription;

}