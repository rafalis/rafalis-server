package ch.picportos.common;

public class Constants {
    public static final String DEFAULT_DATE = null;
    public static final String DEFAULT_DESCRIPTION = "-";
    public static final String DEFAULT_TAG = "";
    public static final String DEFAULT_AUTHOR = "-";
    public static final String DEFAULT_CAMERA = "-";
    public static final String DEFAULT_MAIN_TITLE = "My Gallery";
    public static final String DEFAULT_SUB_TITLE = "-";

    public static final String SYSTEM_MAP_NAME = "Rafalis map";
    public static final String SYSTEM_MAP_FILENAME = "rafalis_system_map";


    public static final int IMAGE_SMALL_MIN_WIDTH = 1300;
    public static final int IMAGE_SMALL_MIN_HEIGHT = 1200;
    public static final int THUMBNAIL_MIN_WIDTH = 1000;
    public static final int THUMBNAIL_MIN_HEIGHT = 600;
    public static final int MINITHUMBNAIL_MIN_WIDTH = 100;
    public static final int MINITHUMBNAIL_MIN_HEIGHT = 100;
    public static final int COLORS_IN_PALETTE = 5;
    public static final String DEFAULT_MAP_NAME = "default-map";

    public static final int[] IMAGE_QUALITIES = {0, 1, 2, 3};
}
