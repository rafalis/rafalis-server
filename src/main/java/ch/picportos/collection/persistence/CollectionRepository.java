package ch.picportos.collection.persistence;

import ch.picportos.auth.Authentication;
import ch.picportos.collection.api.model.Collection;
import ch.picportos.collection.api.model.CollectionSummary;
import ch.picportos.collection.api.model.CollectionUpdateStatus;
import ch.picportos.collection.persistence.model.CollectionDB;
import ch.picportos.common.errors.ForbiddenException;
import ch.picportos.common.errors.NotFoundException;
import ch.picportos.img.persistence.ImageRepository;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ApplicationScoped
@JBossLog
public class CollectionRepository implements PanacheRepository<CollectionDB> {

    @Inject
    EntityManager entityManager;

    @Inject
    ImageRepository imageRepository;

    @Inject
    Authentication auth;

    @Transactional
    public Collection getCollection(Long id) {
        Collection collection = entityManager.createQuery("SELECT new ch.picportos.collection.api.model.Collection(" +
                        "c.id," +
                        "c.name," +
                        "c.description," +
                        "c.thumbnailid," +
                        "c.structure," +
                        "c.textContent," +
                        "c.imageContent," +
                        "c.carouselContent," +
                        "c.ispublic," +
                        "c.map_id," +
                        "c.lastModified)" +
                        "FROM CollectionDB c " +
                        "WHERE c.id = :reqId", Collection.class)
                .setParameter("reqId", id)
                .getSingleResult();
        if (!auth.isAuthenticated() && !collection.isIspublic()) {
            throw new ForbiddenException("Requesting a non public collection while not authenticated is forbidden.");
        }
        return collection;
    }

    @Transactional
    public CollectionUpdateStatus updateCollection(Long id, String name, String structure, List<String> textContent,
                                                   List<Long> imageContent, String description, Long thumbnailid,
                                                   boolean ispublic, List<List<Long>> carouselContent) {
        auth.ensureIsAdmin();
        log.info("[collection update] " + id.toString() + "  " + name + "  " + structure);
        CollectionDB collection = entityManager.find(CollectionDB.class, id);
        imageRepository.tagCollectionToImages(imageContent, carouselContent, collection);
        collection.setName(name);
        collection.setStructure(structure);
        collection.setTextContent(textContent);
        collection.setImageContent(imageContent);
        collection.setDescription(description);
        collection.setThumbnailid(thumbnailid);
        collection.setIspublic(ispublic);
        collection.setCarouselContent(carouselContent);
        collection.setLastModified(System.currentTimeMillis());
        return new CollectionUpdateStatus("success");
    }

    @Transactional
    public boolean insertCollection(CollectionDB newCollection) {
        auth.ensureIsAdmin();
        if (find("name", newCollection.getName()).singleResultOptional().isEmpty()) {
            entityManager.persist(newCollection);
            log.info("Inserted 1 collection successfully : " + newCollection.getName());
            return true;
        }
        log.warn(newCollection.getName() + " not inserted - already exists !");
        throw new NotFoundException("Could not add your collection to the database : " + newCollection.getName() + " " +
                "already exists !");
    }

    @Transactional
    public boolean deleteCollection(Long id) {
        auth.ensureIsAdmin();
        log.info("Un-tagging collection from collection images");
        imageRepository.tagCollectionToImages(new ArrayList<>(), new ArrayList<>(), findById(id));
        log.info("Deleting collection with id #" + id);
        return deleteById(id);
    }

    @Transactional
    public List<CollectionSummary> getCollectionSummary() {
        Stream<CollectionSummary> collectionSummaries = entityManager.createQuery("SELECT new ch.picportos.collection" +
                        ".api.model.CollectionSummary(" +
                        "c.id," +
                        "c.name," +
                        "c.description," +
                        "(SELECT i.id FROM ImagesDB i WHERE c.thumbnailid = i.id)," +
                        "c.ispublic," +
                        "c.map_id)" +
                        "FROM CollectionDB c", CollectionSummary.class)
                .getResultStream();
        return collectionSummaries
                .filter(collectionSummary -> collectionSummary.isIspublic() || auth.isAuthenticated())
                .peek(collectionSummary -> {
                    if (!auth.isAuthenticated()) {
                        collectionSummary.setMap_id(null);
                    }
                })
                .collect(Collectors.toList());
    }

    @Transactional
    public void useMapForCollection(Long colId, Long mapId) {
        CollectionDB collectionDB = findById(colId);
        collectionDB.setMap_id(mapId);
    }
}
