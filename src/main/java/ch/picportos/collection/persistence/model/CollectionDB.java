package ch.picportos.collection.persistence.model;

import ch.picportos.common.converters.LongListConverter;
import ch.picportos.common.converters.LongListListConverter;
import ch.picportos.common.converters.StringListConverterEncoded;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "collections")
public class CollectionDB {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String structure;

    private String description;

    private Long thumbnailid;

    @Convert(converter = StringListConverterEncoded.class)
    private List<String> textContent;

    @Convert(converter = LongListConverter.class)
    private List<Long> imageContent;

    @Convert(converter = LongListListConverter.class)
    private List<List<Long>> carouselContent;

    private boolean ispublic;

    private Long map_id;

    private Long lastModified;
}
