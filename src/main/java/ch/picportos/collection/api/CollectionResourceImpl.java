package ch.picportos.collection.api;

import ch.picportos.collection.api.model.Collection;
import ch.picportos.collection.api.model.CollectionSummary;
import ch.picportos.collection.api.model.CollectionUpdateStatus;
import ch.picportos.collection.persistence.CollectionRepository;
import ch.picportos.collection.persistence.model.CollectionDB;
import ch.picportos.common.errors.ApiException;
import ch.picportos.common.errors.NotFoundException;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

@JBossLog
public class CollectionResourceImpl implements CollectionResource {

    @Inject
    CollectionRepository collectionRepository;

    @Override
    public Collection getCollection(Long id) {
        try {
            return collectionRepository.getCollection(id);
        } catch (NoResultException e) {
            log.warn("Collection with id #" + id + " was not found !");
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    @Override
    public List<CollectionSummary> getCollectionSummary() {
        return this.collectionRepository.getCollectionSummary();
    }

    @Override
    public CollectionUpdateStatus modifyCollection(Long id, Collection newCollection) {
        this.ensureCollectionIsWellFormed(newCollection);
        try {
            assert Objects.equals(id, newCollection.getId());
            return collectionRepository.updateCollection(
                    id,
                    newCollection.getName(),
                    newCollection.getStructure(),
                    newCollection.getTextContent(),
                    newCollection.getImageContent(),
                    newCollection.getDescription(),
                    newCollection.getThumbnailid(),
                    newCollection.isIspublic(),
                    newCollection.getCarouselContent()
            );
        } catch (NullPointerException | NoResultException e) {
            log.warn("Collection #" + id + " could not be updated!");
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    @Override
    public CollectionUpdateStatus addCollection(Collection newCollection) {
        this.ensureCollectionIsWellFormed(newCollection);
        CollectionDB newCollectionDB = new CollectionDB();
        newCollectionDB.setName(newCollection.getName());
        newCollectionDB.setStructure(newCollection.getStructure());
        newCollectionDB.setDescription(newCollection.getDescription());
        newCollectionDB.setThumbnailid(newCollection.getThumbnailid());
        newCollectionDB.setTextContent(newCollection.getTextContent());
        newCollectionDB.setImageContent(newCollection.getImageContent());
        newCollectionDB.setIspublic(newCollection.isIspublic());
        newCollectionDB.setLastModified(System.currentTimeMillis());
        boolean res = collectionRepository.insertCollection(newCollectionDB);
        if (!res) {
            log.warn("An error occured while creating new collection " + newCollection.getName());
        }
        return new CollectionUpdateStatus(res ? "success" : "failed");
    }

    @Override
    public Response deleteCollection(Long id) {
        if (collectionRepository.deleteCollection(id)) {
            return Response.ok().build();
        }
        throw new NotFoundException("Collection could not be removed");
    }

    private void ensureCollectionIsWellFormed(Collection newCollection) {
        int texts = StringUtils.countMatches(newCollection.getStructure(), "T");
        int images = StringUtils.countMatches(newCollection.getStructure(), "I");
        int carousels = StringUtils.countMatches(newCollection.getStructure(), "C");

        if (newCollection.getTextContent().size() != texts ||
                newCollection.getImageContent().size() != images ||
                newCollection.getCarouselContent().size() != carousels) {
            log.warn("Collection " + newCollection.getName() + " was malformed, rejecting request.");
            throw new ApiException(Response.Status.BAD_REQUEST,
                    "Collection is malformed (structure does not match text and image contents)");
        }
    }
}
