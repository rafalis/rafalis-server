package ch.picportos.collection.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Collection {
    private Long id;

    private String name;

    private String description;

    private Long thumbnailid;

    private String structure;

    private List<String> textContent;

    private List<Long> imageContent;

    private List<List<Long>> carouselContent;

    private boolean ispublic;

    private Long map_id;

    private Long lastModified;

}

