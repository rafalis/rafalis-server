package ch.picportos.collection.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class CollectionSummary {
    private Long id;

    private String name;

    private String description;

    private Long thumbnailid;

    private boolean ispublic;

    private Long map_id;
}
