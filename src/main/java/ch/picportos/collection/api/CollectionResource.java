package ch.picportos.collection.api;

import ch.picportos.collection.api.model.Collection;
import ch.picportos.collection.api.model.CollectionSummary;
import ch.picportos.collection.api.model.CollectionUpdateStatus;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Tag(name = "Collections management")
@Path("/collection")
public interface CollectionResource {

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieve a collection by id")
    Collection getCollection(@PathParam("id") Long id);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieve summary of all the collections created")
    List<CollectionSummary> getCollectionSummary();

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Modify a collection by id")
    CollectionUpdateStatus modifyCollection(@PathParam("id") Long id, Collection newCollection);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Add a new collection")
    CollectionUpdateStatus addCollection(Collection newCollection);

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Delete a collection by id")
    Response deleteCollection(@PathParam("id") Long id);

}
