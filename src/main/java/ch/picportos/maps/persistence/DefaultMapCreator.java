package ch.picportos.maps.persistence;

import ch.picportos.common.Constants;
import ch.picportos.maps.persistence.model.MapDB;
import com.mapbox.geojson.FeatureCollection;
import io.quarkus.runtime.StartupEvent;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Optional;

@JBossLog
@ApplicationScoped
public class DefaultMapCreator {

    @Inject
    MapsRepository mapsRepository;

    void onStart(@Observes StartupEvent startupEvent) {
        Optional<MapDB> mapDB = mapsRepository.find("name", Constants.SYSTEM_MAP_NAME).singleResultOptional();
        if (mapDB.isEmpty()) {
            createSystemMap();
            log.info("Created default system map " + Constants.SYSTEM_MAP_NAME);
        } else {
            mapsRepository.setDefaultMapID(mapDB.get().getId());
        }
    }

    @Transactional
    public void createSystemMap() {
        MapDB systemMap = new MapDB();
        systemMap.setName(Constants.SYSTEM_MAP_NAME);
        systemMap.setFilename(Constants.SYSTEM_MAP_FILENAME);
        systemMap.setReferenced_images(new ArrayList<>());
        systemMap.setReferenced_collection(null);
        FeatureCollection fc = FeatureCollection.fromJson("{type: 'FeatureCollection', features: []}");
        systemMap.setGeojson(fc.toJson().getBytes(StandardCharsets.UTF_8));
        mapsRepository.persist(systemMap);
    }

}
