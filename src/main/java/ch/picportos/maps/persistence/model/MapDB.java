package ch.picportos.maps.persistence.model;

import ch.picportos.common.converters.LongListConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "MAPS")
public class MapDB {
    @GeneratedValue
    @Id
    private Long id;

    @NotBlank
    private String filename;

    @NotBlank
    private String name;

    @NotBlank
    private byte[] geojson;

    @Convert(converter = LongListConverter.class)
    private List<Long> referenced_images;

    private Long referenced_collection;
}
