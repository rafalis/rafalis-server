package ch.picportos.maps.persistence;

import ch.picportos.auth.Authentication;
import ch.picportos.collection.persistence.CollectionRepository;
import ch.picportos.common.Constants;
import ch.picportos.common.errors.ApiException;
import ch.picportos.common.errors.NotFoundException;
import ch.picportos.img.persistence.ImageRepository;
import ch.picportos.maps.api.model.MapSummary;
import ch.picportos.maps.api.model.MapUpdateStatus;
import ch.picportos.maps.persistence.model.MapDB;
import com.google.gson.JsonElement;
import com.mapbox.geojson.FeatureCollection;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import lombok.Setter;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@JBossLog
@ApplicationScoped
public class MapsRepository implements PanacheRepository<MapDB> {
    @Inject
    EntityManager entityManager;

    @Inject
    Authentication auth;

    @Inject
    ImageRepository imageRepository;

    @Inject
    CollectionRepository collectionRepository;

    @Setter
    private Long defaultMapID;

    @Transactional
    public boolean insertMap(MapDB newMap) {
        auth.ensureIsAdmin();
        ensureMapIsNotConflictingWithSystemMap(newMap.getName(), newMap.getFilename());
        List<Long> referencedImages;
        try {
            referencedImages = this.parseMap(newMap.getGeojson());
        } catch (Exception e) {
            throw new ApiException(Response.Status.BAD_REQUEST, "Malformed Json file !");
        }
        newMap.setReferenced_images(referencedImages);
        if (this.findByFileName(newMap.getFilename()).isEmpty()) {
            entityManager.persist(newMap);
            imageRepository.tagMapToImages(referencedImages,
                    new ArrayList<>(), this.findByFileName(newMap.getFilename()).get().getId());
            log.info("Inserted 1 map successfully : " + newMap.getName());
            return true;
        }
        log.warn(newMap.getFilename() + " not inserted, name already exists !");
        throw new ApiException(Response.Status.BAD_REQUEST,
                newMap.getFilename() + "' already exists !");
    }

    private void ensureMapIsNotConflictingWithSystemMap(String mapName, String filename) {
        if (mapName.equals(Constants.SYSTEM_MAP_NAME) || filename.equals(Constants.SYSTEM_MAP_FILENAME)) {
            throw new ApiException(
                    Response.Status.BAD_REQUEST,
                    "Name " + Constants.SYSTEM_MAP_NAME + "," + Constants.SYSTEM_MAP_FILENAME + " is reserved.");
        }
    }

    private List<Long> parseMap(byte[] geoJsonByteArray) {
        FeatureCollection geoJson = FeatureCollection.fromJson(new String(geoJsonByteArray));
        assert geoJson.features() != null;
        HashSet<Long> imageReferences = new HashSet<>();
        geoJson.features().forEach(feature -> {
            assert feature.geometry() != null && feature.properties() != null;
            JsonElement desc = feature.properties().get("description");
            if (desc != null && desc.getAsString().contains("!img")) {
                Pattern pattern = Pattern.compile("!img([0-9]+)");
                Matcher matcher = pattern.matcher(desc.getAsString());
                if (matcher.find()) {
                    log.info("Feature " + feature.geometry().type() + ": " + desc);
                    imageReferences.add(Long.valueOf(matcher.group(1)));
                }
            }
        });
        log.info("Image(s) reference(s) found : " + imageReferences);
        return new ArrayList<>(imageReferences);
    }

    private Optional<MapDB> findByFileName(String filename) {
        return find("filename", filename).singleResultOptional();
    }

    @Transactional
    public byte[] getGeoJson(Long id) {
        MapDB mapDB;
        if (id == -1) {
            mapDB = this.getSystemMap();
        } else {
            mapDB = this.findById(id);
        }
        if (mapDB == null) {
            throw new NotFoundException();
        }
        return mapDB.getGeojson();
    }

    @Transactional
    public MapDB getSystemMap() {
        Optional<MapDB> mapDB = findByName(Constants.SYSTEM_MAP_NAME);
        if (mapDB.isPresent()) {
            return mapDB.get();
        }
        throw new NotFoundException("System map was not found !");
    }

    private Optional<MapDB> findByName(String name) {
        return find("name", name).singleResultOptional();
    }

    @Transactional
    public List<MapSummary> getMapSummaries() {
        return entityManager.createQuery("SELECT new ch.picportos.maps.api.model.MapSummary(" +
                        "m.id," +
                        "m.name," +
                        "m.filename," +
                        "m.referenced_images," +
                        "m.referenced_collection) " +
                        "FROM MapDB m", MapSummary.class)
                .getResultList();
    }

    @Transactional
    public MapUpdateStatus modifyMap(Long id, String name, String fileName, byte[] newGeoJSon, Long newCollectionId) {
        auth.ensureIsAdmin();
        MapDB mapDB;
        boolean isUpdatingSystemMap = Objects.equals(id, defaultMapID);
        if (id == -1) {
            mapDB = this.getSystemMap();
        } else {
            mapDB = this.findById(id);
        }
        if (!isUpdatingSystemMap) {
            mapDB.setName(name);
        }
        if (newCollectionId != 0) {
            Long collectionId = newCollectionId == -1 ? null : newCollectionId;
            Long oldCollectionId = mapDB.getReferenced_collection();
            if (oldCollectionId != null) {
                collectionRepository.useMapForCollection(oldCollectionId, null);
            }
            if (collectionId != null) {
                collectionRepository.useMapForCollection(collectionId, id);
            }
            mapDB.setReferenced_collection(collectionId);
        }
        if (newGeoJSon.length > 0) {
            List<Long> newImageIDs;
            try {
                newImageIDs = parseMap(newGeoJSon);
            } catch (Exception e) {
                throw new ApiException(Response.Status.BAD_REQUEST, "Malformed Json file !");
            }
            imageRepository.tagMapToImages(newImageIDs, mapDB.getReferenced_images(), id);
            mapDB.setGeojson(newGeoJSon);
            if (!fileName.isEmpty() && !isUpdatingSystemMap) {
                mapDB.setFilename(fileName);
            }
            mapDB.setReferenced_images(newImageIDs);
        }
        return new MapUpdateStatus("success");
    }

    @Transactional
    public Response deleteMap(Long id) {
        auth.ensureIsAdmin();
        MapDB toDelete = findById(id);
        if (toDelete.getFilename().equals(Constants.SYSTEM_MAP_FILENAME)) {
            throw new ApiException(Response.Status.BAD_REQUEST, "You cannot delete the system map.");
        }
        Long referencedCollectionId = toDelete.getReferenced_collection();
        if (referencedCollectionId != null) {
            collectionRepository.useMapForCollection(referencedCollectionId, null);
        }
        imageRepository.tagMapToImages(new ArrayList<>(), toDelete.getReferenced_images(), id);
        if (this.deleteById(id)) {
            return Response.ok().build();
        }
        throw new NotFoundException("Map " + id + " was not found, could not delete it");
    }

    public MapSummary getMapSummary(Long id) {
        MapDB mapDB;
        if (id == -1) {
            mapDB = this.getSystemMap();
        } else {
            mapDB = findById(id);
        }
        return new MapSummary(
                mapDB.getId(),
                mapDB.getName(),
                mapDB.getFilename(),
                mapDB.getReferenced_images(),
                mapDB.getReferenced_collection()
        );
    }
}
