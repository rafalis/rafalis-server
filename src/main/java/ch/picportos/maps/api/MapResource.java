package ch.picportos.maps.api;

import ch.picportos.common.multipart.MultipartBodyMap;
import ch.picportos.maps.api.model.MapAddedConfirmation;
import ch.picportos.maps.api.model.MapSummary;
import ch.picportos.maps.api.model.MapUpdateStatus;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Tag(name = "Maps to display")
@Path("/map")
public interface MapResource {
    @GET
    @Path("sum")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieve all map summaries")
    List<MapSummary> getMaps();

    @GET
    @Path("{id}/geojson")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(summary = "Retrieve a geojson associated with an id")
    Response getGeoJson(@PathParam("id") Long id);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Insert a new map")
    MapAddedConfirmation insertMap(@MultipartForm MultipartBodyMap formData);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    @Operation(summary = "Get a specific map summary from an ID")
    MapSummary getMapSummary(@PathParam("id") Long id);

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Update a map")
    @Path("{id}")
    MapUpdateStatus modifyMap(@MultipartForm MultipartBodyMap formData, @PathParam("id") Long id);

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Delete a map and remove its references from images")
    Response deleteMap(@PathParam("id") Long id);
}
