package ch.picportos.maps.api;

import ch.picportos.common.Constants;
import ch.picportos.common.errors.ApiException;
import ch.picportos.common.errors.NotFoundException;
import ch.picportos.common.multipart.MultipartBodyMap;
import ch.picportos.maps.api.model.MapAddedConfirmation;
import ch.picportos.maps.api.model.MapSummary;
import ch.picportos.maps.api.model.MapUpdateStatus;
import ch.picportos.maps.persistence.MapsRepository;
import ch.picportos.maps.persistence.model.MapDB;
import lombok.extern.jbosslog.JBossLog;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@JBossLog
public class MapResourceImpl implements MapResource {

    @Inject
    MapsRepository mapsRepository;

    @Override
    public List<MapSummary> getMaps() {
        return mapsRepository.getMapSummaries();
    }

    @Override
    public Response getGeoJson(Long id) {
        try {
            byte[] geoJson = mapsRepository.getGeoJson(id);
            return Response.ok(geoJson).type(MediaType.APPLICATION_JSON).build();
        } catch (NoResultException e) {
            log.warn("Map with id #" + id + " was not found !");
            throw new NotFoundException("Map with id #" + id + " was not found !");
        }
    }

    @Override
    public MapAddedConfirmation insertMap(MultipartBodyMap formData) {
        MapDB newMap = createMapFromInputStream(formData.file, formData.name, formData.filename,
                Long.parseLong(formData.collectionID));
        if (newMap != null) {
            boolean mapInserted = this.mapsRepository.insertMap(newMap);
            if (mapInserted) {
                Long newMapID = this.mapsRepository.find("filename", formData.filename).singleResult().getId();
                return new MapAddedConfirmation(newMapID, "success");
            }
        }
        throw new ApiException(Response.Status.BAD_REQUEST, "Failed to insert new map");
    }

    @Override
    public MapSummary getMapSummary(Long id) {
        try {
            return mapsRepository.getMapSummary(id);
        } catch (NoResultException e) {
            log.warn("Map with id #" + id + " was not found !");
            throw new NotFoundException("Map with id #" + id + " was not found !");
        }
    }

    @Override
    public MapUpdateStatus modifyMap(@MultipartForm MultipartBodyMap formData, Long id) {
        byte[] newGeoJson = new byte[0];
        if (formData.file != null) {
            try {
                newGeoJson = formData.file.readAllBytes();
            } catch (IOException e) {
                throw new ApiException(Response.Status.BAD_REQUEST, "IO error in MapResourceImpl.");
            }
        }
        return this.mapsRepository.modifyMap(id, formData.name, formData.filename, newGeoJson,
                parseCollectionId(formData.collectionID));
    }

    private Long parseCollectionId(String in) {
        if (in.equals("null") || in.isBlank()) {
            return 0L;
        }
        return Long.parseLong(in);
    }

    @Transactional
    public Response deleteMap(Long id) {
        return this.mapsRepository.deleteMap(id);
    }

    private MapDB createMapFromInputStream(InputStream is, String name, String filename, Long referenced_collection) {
        try {
            MapDB mapDB = new MapDB();
            mapDB.setName(name.isEmpty() ? Constants.DEFAULT_MAP_NAME : name);
            mapDB.setFilename(filename);
            mapDB.setGeojson(is.readAllBytes());
            // -1 for null, 0 for "no changes"
            if (referenced_collection > 0) {
                mapDB.setReferenced_collection(referenced_collection);
            }
            return mapDB;
        } catch (IOException e) {
            log.warn("Could not create the map " + name + ": " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
}
