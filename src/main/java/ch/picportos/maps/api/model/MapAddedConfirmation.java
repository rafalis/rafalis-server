package ch.picportos.maps.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class MapAddedConfirmation {
    private Long id;
    private String message;
}
