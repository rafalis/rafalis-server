package ch.picportos.maps.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class MapSummary {
    private Long id;
    private String name;
    private String filename;
    private List<Long> referenced_images;
    private Long referenced_collection;
}
