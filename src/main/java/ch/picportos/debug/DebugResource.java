/*package ch.picportos.debug;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Tag(name = "Debug tools")
@Path("/debug")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface DebugResource {

    @GET
    @Path("debug/{id}")
    @Operation(summary = "Trigger debug resource for tests")
    Response debug(@PathParam("id") Long id);
}
*/