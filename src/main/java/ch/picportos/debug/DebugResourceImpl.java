/*package ch.picportos.debug;

import ch.picportos.auth.Authentication;
import ch.picportos.common.Constants;
import ch.picportos.common.errors.ApiException;
import ch.picportos.img.persistence.ImageRepository;
import ch.picportos.img.persistence.model.ImagesDB;
import ch.picportos.img.process.ImageProcessor;
import io.quarkus.runtime.configuration.ProfileManager;
import lombok.extern.jbosslog.JBossLog;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.*;

@Tag(name = "Debug tools")
@Path("/debug")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@JBossLog
public class DebugResourceImpl implements DebugResource {

    @Inject
    ImageRepository imageRepository;

    @Inject
    Authentication authentication;

    private void ensureEnvIsDev() {
        if (!"dev".equals(ProfileManager.getActiveProfile())) {
            throw new ForbiddenException("This method can only be called in dev environment.");
        }
    }

    @Override
    @Transactional
    public Response debug(Long id) {
//        ensureEnvIsDev();
        authentication.ensureIsAdmin();
        ImagesDB imagesDB = imageRepository.findById(id);
        if (imagesDB == null) {
            throw new ApiException(Response.Status.BAD_REQUEST, "Id does not exists");
        }
        log.info("#" + imagesDB.getId() + " - " + imagesDB.getTitle());
        try {

            BufferedImage mainImage = ImageIO.read(new ByteArrayInputStream(imagesDB.getImage()));

            byte[] thumbnail;
            byte[] minithumbnail;
            byte[] image_small;
            float baseWidth = mainImage.getWidth();
            float baseHeight = mainImage.getHeight();

            int newWidthThumb, newHeightThumb, newWidthMiniThumb, newHeightMiniThumb, newWidthSmallImage,
                    newHeightSmallImage;
            boolean forceWidth = baseWidth > baseHeight;
            log.info("Image is " + baseWidth + "x" + baseHeight + ", forceWidth=" + forceWidth);

            if (forceWidth) {
                newHeightMiniThumb = Constants.MINITHUMBNAIL_MIN_HEIGHT;
                newWidthMiniThumb = (int) (baseWidth / baseHeight * newHeightMiniThumb);
                newHeightThumb = Constants.THUMBNAIL_MIN_HEIGHT;
                newWidthThumb = (int) (baseWidth / baseHeight * newHeightThumb);
                newHeightSmallImage = Constants.IMAGE_SMALL_MIN_HEIGHT;
                newWidthSmallImage = (int) (baseWidth / baseHeight * newHeightSmallImage);
            } else {
                newWidthMiniThumb = Constants.MINITHUMBNAIL_MIN_WIDTH;
                newHeightMiniThumb = (int) (baseWidth / baseHeight * newWidthMiniThumb);
                newWidthThumb = Constants.THUMBNAIL_MIN_WIDTH;
                newHeightThumb = (int) (baseHeight / baseWidth * newWidthThumb);
                newWidthSmallImage = Constants.IMAGE_SMALL_MIN_WIDTH;
                newHeightSmallImage = (int) (baseHeight / baseWidth * newWidthSmallImage);
            }

            ImageWriter imageWriter = ImageIO.getImageWritersByFormatName("jpeg").next();
            ImageWriteParam jpegImageWriteParam = imageWriter.getDefaultWriteParam();
            jpegImageWriteParam.setProgressiveMode(ImageWriteParam.MODE_DEFAULT);

            log.info("Thumbnails will be " + newWidthSmallImage + "x" + newHeightSmallImage + ", " + newWidthThumb + "x" + newHeightThumb + ", " + newWidthMiniThumb
                    + "x" + newHeightMiniThumb);
            BufferedImage thumb = ImageProcessor.resizeImage(mainImage, newWidthThumb, newHeightThumb);
            ByteArrayOutputStream baosThumb = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosThumb));
            imageWriter.write(null, new IIOImage(thumb, null, null), jpegImageWriteParam);
            thumbnail = baosThumb.toByteArray();

            BufferedImage minithumb = ImageProcessor.resizeImage(mainImage, newWidthMiniThumb, newHeightMiniThumb);
            ByteArrayOutputStream baosMiniThumb = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosMiniThumb));
            imageWriter.write(null, new IIOImage(minithumb, null, null), jpegImageWriteParam);
            minithumbnail = baosMiniThumb.toByteArray();

            BufferedImage smallImage = ImageProcessor.resizeImage(mainImage, newWidthSmallImage, newHeightSmallImage);
            ByteArrayOutputStream baosSmallImage = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosSmallImage));
            imageWriter.write(null, new IIOImage(smallImage, null, null), jpegImageWriteParam);
            image_small = baosSmallImage.toByteArray();

            imagesDB.setSmall_width(smallImage.getWidth());
            imagesDB.setSmall_height(smallImage.getHeight());
            imagesDB.setThumb_width(thumb.getWidth());
            imagesDB.setThumb_height(thumb.getHeight());
            imagesDB.setMinithumb_width(minithumb.getWidth());
            imagesDB.setMinithumb_height(minithumb.getHeight());
            imagesDB.setImage_small(image_small);
            imagesDB.setThumb(thumbnail);
            imagesDB.setMini_thumb(minithumbnail);
            log.info("Updated.");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().build();
    }

}
*/