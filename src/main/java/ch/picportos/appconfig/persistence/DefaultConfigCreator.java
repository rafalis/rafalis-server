package ch.picportos.appconfig.persistence;

import ch.picportos.appconfig.api.model.VersionInfo;
import ch.picportos.appconfig.persistence.model.AppConfigDB;
import ch.picportos.common.Constants;
import io.quarkus.runtime.StartupEvent;
import lombok.extern.jbosslog.JBossLog;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;

@JBossLog
@ApplicationScoped
public class DefaultConfigCreator {

    @Inject
    AppConfigRepository appConfigRepository;

    @ConfigProperty(name = "rafalis.release.version")
    String releaseVersion;

    @ConfigProperty(name = "rafalis.release.date")
    String releaseDate;

    void onStart(@Observes StartupEvent startupEvent) {
        int current_size =
                appConfigRepository.getEntityManager().createNativeQuery("SELECT 1 from appconfig").getResultList().size();
        if (current_size == 0) {
            createDefaultAppConfig();
        }
        appConfigRepository.setVersionInfo(new VersionInfo(releaseVersion, releaseDate));
    }

    @Transactional
    public void createDefaultAppConfig() {
        AppConfigDB appConfigDB = new AppConfigDB();
        ArrayList<String> authors = new ArrayList<>(1);
        ArrayList<String> cameras = new ArrayList<>(1);
        authors.add(Constants.DEFAULT_AUTHOR);
        cameras.add(Constants.DEFAULT_CAMERA);
        appConfigDB.setAuthors(authors);
        appConfigDB.setCameras(cameras);
        appConfigDB.setMain_title(Constants.DEFAULT_MAIN_TITLE);
        appConfigDB.setSub_title(Constants.DEFAULT_SUB_TITLE);
        appConfigRepository.persist(appConfigDB);
        log.info("Created default app config Authors: " + Constants.DEFAULT_AUTHOR +
                ", Cameras: " + Constants.DEFAULT_CAMERA +
                ", Main Title: " + Constants.DEFAULT_MAIN_TITLE +
                ", Sub Title: " + Constants.DEFAULT_SUB_TITLE);
    }
}
