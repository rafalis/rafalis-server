package ch.picportos.appconfig.persistence;

import ch.picportos.appconfig.api.model.AdminConfig;
import ch.picportos.appconfig.api.model.AdminConfigUpdateStatus;
import ch.picportos.appconfig.api.model.VersionInfo;
import ch.picportos.appconfig.persistence.model.AppConfigDB;
import ch.picportos.auth.Authentication;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@JBossLog
@ApplicationScoped
public class AppConfigRepository implements PanacheRepository<AppConfigDB> {

    @Inject
    Authentication auth;

    @Inject
    EntityManager entityManager;

    @Getter
    @Setter
    private VersionInfo versionInfo;

    public AdminConfig getConfig() {
        AppConfigDB adminConfigDB = this.getConfigInDatabase();
        return new AdminConfig(adminConfigDB.getAuthors(),
                adminConfigDB.getCameras(), adminConfigDB.getMain_title(), adminConfigDB.getSub_title());
    }

    private AppConfigDB getConfigInDatabase() {
        String sql = "SELECT t FROM AppConfigDB t";
        Query query = entityManager.createQuery(sql, AppConfigDB.class);
        query.setMaxResults(1);
        return (AppConfigDB) query.getResultList().get(0);
    }

    @Transactional
    public AdminConfigUpdateStatus modifyConfig(List<String> authors, List<String> cameras, String mainTitle, String subTitle) {
        auth.ensureIsAdmin();
        log.info("New config - " + authors + " - " + cameras + " - " + mainTitle + " - " + subTitle);
        AppConfigDB appConfigDB = this.getConfigInDatabase();
        appConfigDB.setAuthors(authors);
        appConfigDB.setCameras(cameras);
        if (mainTitle != null) {
            appConfigDB.setMain_title(mainTitle);
        }
        if (subTitle != null) {
            appConfigDB.setSub_title(subTitle);
        }
        return new AdminConfigUpdateStatus("success");
    }

    public VersionInfo retrieveVersionInfo() {
        return versionInfo;
    }
}
