package ch.picportos.appconfig.persistence.model;

import ch.picportos.common.converters.StringListConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "APPCONFIG")
public class AppConfigDB {
    @GeneratedValue
    @Id
    private Long id;

    @Convert(converter = StringListConverter.class)
    private List<String> authors;

    @Convert(converter = StringListConverter.class)
    private List<String> cameras;

    private String main_title;

    private String sub_title;
}
