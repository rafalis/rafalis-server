package ch.picportos.appconfig.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class AdminConfigUpdateStatus {
    private String message;
}
