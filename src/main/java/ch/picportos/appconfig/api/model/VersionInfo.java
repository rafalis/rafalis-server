package ch.picportos.appconfig.api.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class VersionInfo {
    private String version;
    private String date;
}
