package ch.picportos.appconfig.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AdminConfig {
    private List<String> authors;
    private List<String> cameras;
    private String mainTitle;
    private String subTitle;
}
