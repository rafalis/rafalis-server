package ch.picportos.appconfig.api;

import ch.picportos.appconfig.api.model.AdminConfig;
import ch.picportos.appconfig.api.model.AdminConfigUpdateStatus;
import ch.picportos.appconfig.api.model.VersionInfo;
import ch.picportos.appconfig.persistence.AppConfigRepository;

import javax.inject.Inject;

public class AppConfigResourceImpl implements AppConfigResource {

    @Inject
    AppConfigRepository appConfigRepository;

    @Override
    public AdminConfig getConfig() {
        return appConfigRepository.getConfig();
    }

    @Override
    public AdminConfigUpdateStatus modifyConfig(AdminConfig adminConfig) {
        return appConfigRepository.modifyConfig(adminConfig.getAuthors(), adminConfig.getCameras(),
                adminConfig.getMainTitle(), adminConfig.getSubTitle());
    }

    @Override
    public VersionInfo getVersionInfo() { return  appConfigRepository.retrieveVersionInfo(); }
}
