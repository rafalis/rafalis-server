package ch.picportos.appconfig.api;


import ch.picportos.appconfig.api.model.AdminConfig;
import ch.picportos.appconfig.api.model.AdminConfigUpdateStatus;
import ch.picportos.appconfig.api.model.VersionInfo;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Tag(name = "User application configuration management")
@Path("appconfig")
public interface AppConfigResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    AdminConfig getConfig();

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    AdminConfigUpdateStatus modifyConfig(AdminConfig adminConfig);

    @GET
    @Path("version")
    @Produces(MediaType.APPLICATION_JSON)
    VersionInfo getVersionInfo();
}
