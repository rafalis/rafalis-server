package ch.picportos.img.process;

import ch.picportos.common.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jbosslog.JBossLog;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

@JBossLog
@Getter
@Setter
public class ImageProcessor {

    public static BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) {
        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();
        return resizedImage;
    }

    public static ArrayList<String> getPalette(BufferedImage sourceImage) {
        ArrayList<String> paletteDB = new ArrayList<>(Constants.COLORS_IN_PALETTE);
        int[][] palette = ColorThief.getPalette(sourceImage, Constants.COLORS_IN_PALETTE);
        if (palette != null) {
            for (int[] c : palette) {
                paletteDB.add(String.format("#%02x%02x%02x", c[0], c[1], c[2]));
            }
        } else {
            for (int i = 0; i < Constants.COLORS_IN_PALETTE; i++) {
                paletteDB.add("#000000");
            }
        }
        return paletteDB;
    }

    public static BufferedImage rotateImage(BufferedImage original, int rotationInDeg) {
        double rads = Math.toRadians(rotationInDeg);
        double sin = Math.abs(Math.sin(rads));
        double cos = Math.abs(Math.cos(rads));
        int w = (int) Math.floor(original.getWidth() * cos + original.getHeight() * sin);
        int h = (int) Math.floor(original.getHeight() * cos + original.getWidth() * sin);
        BufferedImage rotatedImage = new BufferedImage(w, h, original.getType());
        AffineTransform at = new AffineTransform();
        at.translate(w / 2.0, h / 2.0);
        at.rotate(rads, 0, 0);
        at.translate(-original.getWidth() / 2.0, -original.getHeight() / 2.0);
        AffineTransformOp rotateOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        rotateOp.filter(original, rotatedImage);
        return rotatedImage;
    }
}
