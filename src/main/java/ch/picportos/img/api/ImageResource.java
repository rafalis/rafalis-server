package ch.picportos.img.api;

import ch.picportos.common.multipart.MultipartBodyImage;
import ch.picportos.img.api.model.*;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Tag(name = "Images and descriptions management")
@Path("img")
public interface ImageResource {
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(summary = "Retrieve image raw bytes from an ID")
    Response getImage(@PathParam("id") Long id, @QueryParam("q") Integer quality);

    @GET
    @Path("random")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Operation(summary = "Retrieve a random image raw bytes among images with tag 'mainpage' for the main page")
    ImageDescription getRandomDescription();

    @GET
    @Path("desc/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieve an image description from an ID")
    ImageDescription getImageDescription(@PathParam("id") Long id);

    @PUT
    @Path("desc/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update an image description from an ID")
    ImageDescriptionUpdateStatus updateImageDescription(@PathParam("id") Long id, ImageDescriptionUpdate newDescription);

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Remove an image from the database")
    Response deleteImage(@PathParam("id") Long id);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Operation(summary = "Add a new image to the database")
    ImageAddedStatus addNewImage(@MultipartForm MultipartBodyImage formData);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Retrieve a list of ImageItem (summaries) containing their thumbs")
    List<ImageItem> getImageList(@QueryParam("keywords") String keywords);

    @POST
    @Path("{id}/rotate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Rotate an image thumbnail")
    ImageDescriptionUpdateStatus rotateImage(@PathParam("id") Long id, @QueryParam("rot") Integer rotationDeg,
                                             @QueryParam("q") List<Integer> qualities);

}
