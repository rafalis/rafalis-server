package ch.picportos.img.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ImageDescription {

    private Long id;

    private String title;

    private String name;

    private String description;

    private String author;

    private String datetime;

    private String camera;

    private String tags; // separated by ,

    private List<String> palette;

    private List<Long> collections_appearance;

    private boolean ispublic;

    private Long map_appearance;

    private boolean show_in_browse;

    private boolean show_in_home;
}
