package ch.picportos.img.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageDescriptionUpdate {
    private String title;

    private String description;

    private String author;

    private String datetime;

    private String camera;

    private String tags; // separated by ,

    private boolean ispublic;

    private boolean show_in_browse;

    private boolean show_in_home;
}
