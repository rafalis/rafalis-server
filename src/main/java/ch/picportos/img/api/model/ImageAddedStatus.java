package ch.picportos.img.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ImageAddedStatus {
    private String message;
    private Long id;
}
