package ch.picportos.img.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class ImageBytes {
    private Long id;
    private byte[] image;
    private boolean ispublic;
}
