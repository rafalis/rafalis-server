package ch.picportos.img.api.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@AllArgsConstructor
public class ImageItem {
    private Long id;

    private String title;

    private String datetime;

    private boolean ispublic;

    private boolean show_in_browse;

    private boolean show_in_home;

    private List<String> palette;

    private String tags;

    private Integer small_width;
    private Integer small_height;
    private Integer thumb_width;
    private Integer thumb_height;
    private Integer minithumb_width;
    private Integer minithumb_height;
}
