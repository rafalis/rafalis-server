package ch.picportos.img.api;

import ch.picportos.common.Constants;
import ch.picportos.common.errors.ApiException;
import ch.picportos.common.errors.NotFoundException;
import ch.picportos.common.multipart.MultipartBodyImage;
import ch.picportos.img.api.model.*;
import ch.picportos.img.persistence.ImageRepository;
import ch.picportos.img.persistence.model.ImagesDB;
import ch.picportos.img.process.ImageProcessor;
import lombok.extern.jbosslog.JBossLog;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.persistence.NoResultException;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@JBossLog
public class ImageResourceImpl implements ImageResource {

    @Inject
    ImageRepository imageRepository;

    @Inject
    Jsonb jsonb;

    public Response getImage(Long id, Integer quality) {
        if (quality == null || IntStream.of(Constants.IMAGE_QUALITIES).noneMatch(n -> n == quality)) {
            throw new ApiException(Response.Status.BAD_REQUEST);
        }
        try {
            byte[] img = imageRepository.getImageBytes(id, quality);
            return Response.ok(img).type("image/jpeg").build();
        } catch (NoResultException e) {
            log.warn("Image with id #" + id + " was not found !");
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    public ImageDescription getRandomDescription() {
        try {
            return imageRepository.getDescription((long) -1);
        } catch (NoResultException e) {
            log.warn("Unable to retrieve a random image : " + e.getMessage());
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    public ImageDescription getImageDescription(Long id) {
        try {
            return imageRepository.getDescription(id);
        } catch (NoResultException e) {
            log.warn("Image with id #" + id + " was not found !");
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    public ImageDescriptionUpdateStatus updateImageDescription(Long id, ImageDescriptionUpdate imageDescription) {
        try {
            return imageRepository.updateImageMeta(
                    id,
                    imageDescription.getCamera(),
                    imageDescription.getTitle(),
                    imageDescription.getAuthor(),
                    imageDescription.getDatetime(),
                    imageDescription.getDescription(),
                    imageDescription.getTags(),
                    imageDescription.isIspublic(),
                    imageDescription.isShow_in_browse(),
                    imageDescription.isShow_in_home()
            );
        } catch (NullPointerException | NoResultException e) {
            log.warn("Description of image #" + id + " could not be updated!");
            throw new ch.picportos.common.errors.NotFoundException();
        }
    }

    @Override
    public Response deleteImage(Long id) {
        if (imageRepository.deleteImage(id)) {
            return Response.ok().build();
        }
        throw new NotFoundException("Image could not be removed");
    }

    public ImageAddedStatus addNewImage(@MultipartForm MultipartBodyImage formData) {
        ImageDescriptionUpdate imageDescription = jsonb.fromJson(formData.imageDescription,
                ImageDescriptionUpdate.class);
        Long id = imageRepository.insertImage(imageFromInputStream(formData.file, formData.name, imageDescription));
        return new ImageAddedStatus("success", id);
    }

    private ImagesDB imageFromInputStream(InputStream is, String fileName, ImageDescriptionUpdate imageDescription) {
        ImagesDB imagesDB = new ImagesDB();
        byte[] picInBytes;
        byte[] thumbnail;
        byte[] minithumbnail;
        byte[] image_small;
        ArrayList<String> paletteDB;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            is.transferTo(baos);
            picInBytes = IOUtils.toByteArray(new ByteArrayInputStream(baos.toByteArray()));
            BufferedImage mainImage = ImageIO.read(new ByteArrayInputStream(baos.toByteArray()));

            paletteDB = ImageProcessor.getPalette(mainImage);

            float baseWidth = mainImage.getWidth();
            float baseHeight = mainImage.getHeight();

            int newWidthThumb, newHeightThumb, newWidthMiniThumb, newHeightMiniThumb, newWidthSmallImage,
                    newHeightSmallImage;
            boolean forceWidth = baseWidth > baseHeight;
            log.info("Image is " + baseWidth + "x" + baseHeight + ", forceWidth=" + forceWidth);

            if (forceWidth) {
                newHeightMiniThumb = Constants.MINITHUMBNAIL_MIN_HEIGHT;
                newWidthMiniThumb = (int) (baseWidth / baseHeight * newHeightMiniThumb);
                newHeightThumb = Constants.THUMBNAIL_MIN_HEIGHT;
                newWidthThumb = (int) (baseWidth / baseHeight * newHeightThumb);
                newHeightSmallImage = Constants.IMAGE_SMALL_MIN_HEIGHT;
                newWidthSmallImage = (int) (baseWidth / baseHeight * newHeightSmallImage);
            } else {
                newWidthMiniThumb = Constants.MINITHUMBNAIL_MIN_WIDTH;
                newHeightMiniThumb = (int) (baseWidth / baseHeight * newWidthMiniThumb);
                newWidthThumb = Constants.THUMBNAIL_MIN_WIDTH;
                newHeightThumb = (int) (baseHeight / baseWidth * newWidthThumb);
                newWidthSmallImage = Constants.IMAGE_SMALL_MIN_WIDTH;
                newHeightSmallImage = (int) (baseHeight / baseWidth * newWidthSmallImage);
            }

            ImageWriter imageWriter = ImageIO.getImageWritersByFormatName("jpeg").next();
            ImageWriteParam jpegImageWriteParam = imageWriter.getDefaultWriteParam();
            jpegImageWriteParam.setProgressiveMode(ImageWriteParam.MODE_DEFAULT);

            log.info("Thumbnails will be " + newWidthSmallImage + "x" + newHeightSmallImage + ", " + newWidthThumb + "x" + newHeightThumb + ", " + newWidthMiniThumb
                    + "x" + newHeightMiniThumb);
            BufferedImage thumb = ImageProcessor.resizeImage(mainImage, newWidthThumb, newHeightThumb);
            ByteArrayOutputStream baosThumb = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosThumb));
            imageWriter.write(null, new IIOImage(thumb, null, null), jpegImageWriteParam);
            thumbnail = baosThumb.toByteArray();

            BufferedImage minithumb = ImageProcessor.resizeImage(mainImage, newWidthMiniThumb, newHeightMiniThumb);
            ByteArrayOutputStream baosMiniThumb = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosMiniThumb));
            imageWriter.write(null, new IIOImage(minithumb, null, null), jpegImageWriteParam);
            minithumbnail = baosMiniThumb.toByteArray();

            BufferedImage smallImage = ImageProcessor.resizeImage(mainImage, newWidthSmallImage, newHeightSmallImage);
            ByteArrayOutputStream baosSmallImage = new ByteArrayOutputStream();
            imageWriter.setOutput(ImageIO.createImageOutputStream(baosSmallImage));
            imageWriter.write(null, new IIOImage(smallImage, null, null), jpegImageWriteParam);
            image_small = baosSmallImage.toByteArray();

            imagesDB.setSmall_width(smallImage.getWidth());
            imagesDB.setSmall_height(smallImage.getHeight());
            imagesDB.setThumb_width(thumb.getWidth());
            imagesDB.setThumb_height(thumb.getHeight());
            imagesDB.setMinithumb_width(minithumb.getWidth());
            imagesDB.setMinithumb_height(minithumb.getHeight());
        } catch (IOException e) {
            log.error("Error while reading or writing image " + fileName);
            throw new ch.picportos.common.errors.NotFoundException("Image " + fileName + " was not found !");
        }

        imagesDB.setName(fileName);
        imagesDB.setTitle(imageDescription.getTitle().isEmpty() ? fileName : imageDescription.getTitle());
        imagesDB.setDescription(imageDescription.getDescription());
        imagesDB.setDatetime(imageDescription.getDatetime());
        imagesDB.setAuthor(imageDescription.getAuthor());
        imagesDB.setCamera(imageDescription.getCamera());
        imagesDB.setTags(imageDescription.getTags());
        imagesDB.setIspublic(imageDescription.isIspublic());
        imagesDB.setShow_in_browse(imageDescription.isShow_in_browse());
        imagesDB.setShow_in_home(imageDescription.isShow_in_home());

        imagesDB.setImage(picInBytes);
        imagesDB.setImage_small(image_small);
        imagesDB.setThumb(thumbnail);
        imagesDB.setMini_thumb(minithumbnail);
        imagesDB.setPalette(paletteDB);

        return imagesDB;
    }

    public List<ImageItem> getImageList(String keywords) {
        ArrayList<String> searchs = new ArrayList<>();
        if (!StringUtils.isBlank(keywords)) {
            searchs = (ArrayList<String>) Arrays.stream(keywords.split(","))
                    .sorted()
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());
        }
        return imageRepository.search(searchs);
    }

    @Override
    public ImageDescriptionUpdateStatus rotateImage(Long id, Integer rotationDeg, List<Integer> qualities) {
        if (rotationDeg == null || qualities.size() == 0) {
            throw new ApiException(Response.Status.BAD_REQUEST, "Specify a rotation and a quality");
        }
        return imageRepository.rotateImage(id, rotationDeg, qualities);
    }
}
