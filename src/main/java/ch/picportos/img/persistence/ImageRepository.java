package ch.picportos.img.persistence;

import ch.picportos.auth.Authentication;
import ch.picportos.collection.persistence.model.CollectionDB;
import ch.picportos.common.errors.ApiException;
import ch.picportos.common.errors.ForbiddenException;
import ch.picportos.common.errors.NotFoundException;
import ch.picportos.img.api.model.ImageBytes;
import ch.picportos.img.api.model.ImageDescription;
import ch.picportos.img.api.model.ImageDescriptionUpdateStatus;
import ch.picportos.img.api.model.ImageItem;
import ch.picportos.img.persistence.model.ImagesDB;
import ch.picportos.img.process.ImageProcessor;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import lombok.extern.jbosslog.JBossLog;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.stream.Collectors;

@JBossLog
@ApplicationScoped
public class ImageRepository implements PanacheRepository<ImagesDB> {
    @Inject
    EntityManager entityManager;

    @Inject
    Authentication auth;

    @Transactional
    public Long insertImage(ImagesDB newImage) {
        auth.ensureIsAdmin();
        if (this.findByName(newImage.getName()).isEmpty()) {
            entityManager.persist(newImage);
            Long id = find("name", newImage.getName()).singleResult().getId();
            log.info("Inserted 1 image successfully : " + newImage.getName() + " with id " + id);
            return id;
        }
        log.warn(newImage.getName() + " not inserted - already exists !");

        throw new ApiException(Response.Status.BAD_REQUEST,
                "Could not add your image to the database : " + newImage.getName() + " already exists !");
    }

    private Optional<ImagesDB> findByName(String name) {
        return find("name", name).singleResultOptional();
    }

    @Transactional
    public boolean deleteImage(Long id) {
        auth.ensureIsAdmin();
        ImagesDB imagesDB = findById(id);
        if (imagesDB.getCollections_appearance().size() > 0) {
            throw new ApiException(Response.Status.BAD_REQUEST, "This image appears in at least one collection." +
                    " First remove it from there.");
        }
        log.info("Deleting image with id #" + id);
        return deleteById(id);
    }

    public List<ImageItem> search(ArrayList<String> keywords) {
//        log.info("Request : " + keywords.toString());
        String search = "%" + String.join(",", keywords).toLowerCase() + "%";
        if (keywords.isEmpty()) {
            search = "%";
        }
//        log.info(search);

        return entityManager.createQuery("SELECT new ch.picportos.img.api.model.ImageItem(" +
                        "i.id," +
                        "i.title," +
                        "i.datetime," +
                        "i.ispublic," +
                        "i.show_in_browse," +
                        "i.show_in_home," +
                        "i.palette," +
                        "i.tags," +
                        "i.small_width," +
                        "i.small_height," +
                        "i.thumb_width," +
                        "i.thumb_height," +
                        "i.minithumb_width," +
                        "i.minithumb_height)" +
                        "FROM ImagesDB i " +
                        "WHERE" +
                        "(LOWER(i.name) IN :search_list " +
                        "OR cast(i.id as string) IN :search_list " +
                        "OR LOWER(i.tags) LIKE :search " +
                        "OR LOWER(i.author) LIKE :search " +
                        "OR LOWER(i.title) LIKE :search " +
                        "OR LOWER(i.camera) LIKE :search)", ImageItem.class)
                .setParameter("search", search)
                .setParameter("search_list", keywords)
                .getResultList()
                .stream()
                .filter((imageItem -> auth.isAuthenticated() || imageItem.isIspublic()))
                .collect(Collectors.toList());
    }

    @Transactional
    public byte[] getImageBytes(Long id, Integer quality) {
        String qualityCol;
        switch (quality) {
            case 0:
                // dont give access to full res image except for the mainpage ones
                if (id != -1) {
                    auth.ensureIsAdmin();
                }
                qualityCol = "i.image";
                break;
            case 1:
                qualityCol = "i.image_small";
                break;
            case 2:
                qualityCol = "i.thumb";
                break;
            case 3:
                qualityCol = "i.mini_thumb";
                break;
            default:
                throw new RuntimeException("Should not go here");
        }
        ImageBytes imageBytes = entityManager.createQuery("SELECT new ch.picportos.img.api.model.ImageBytes(" +
                        "i.id," +
                        qualityCol + "," +
                        "i.ispublic) " +
                        "FROM ImagesDB i " +
                        "WHERE i.id = :requestedId", ImageBytes.class)
                .setParameter("requestedId", id)
                .getSingleResult();
        if (!auth.isAuthenticated() && !imageBytes.isIspublic()) {
            throw new ForbiddenException("Requesting a non public image while not authenticated is forbidden.");
        }
        return imageBytes.getImage();
    }

    @Transactional
    public ImageDescription getDescription(Long id) {
        if (id == -1) {
            List<ImageDescription> imageDescriptions = entityManager.createQuery("SELECT new ch.picportos.img.api.model.ImageDescription" +
                            "(" +
                            "i.id," +
                            "i.title," +
                            "i.name," +
                            "i.description," +
                            "i.author," +
                            "i.datetime," +
                            "i.camera," +
                            "i.tags," +
                            "i.palette," +
                            "i.collections_appearance," +
                            "i.ispublic," +
                            "i.map_appearance," +
                            "i.show_in_browse," +
                            "i.show_in_home) " +
                            "FROM ImagesDB i " +
                            "WHERE i.show_in_home = true " +
                            "AND i.ispublic = true", ImageDescription.class)
                    .getResultList();
            if (imageDescriptions.size() > 0) {
                return imageDescriptions.get(new Random().nextInt(imageDescriptions.size()));
            } else {
                throw new NotFoundException("No public image has the 'show_in_browse' attribute set to true!");
            }
        }
        ImageDescription imageDescription = entityManager.createQuery("SELECT new ch.picportos.img.api.model" +
                        ".ImageDescription(" +
                        "i.id," +
                        "i.title," +
                        "i.name," +
                        "i.description," +
                        "i.author," +
                        "i.datetime," +
                        "i.camera," +
                        "i.tags," +
                        "i.palette," +
                        "i.collections_appearance," +
                        "i.ispublic," +
                        "i.map_appearance," +
                        "i.show_in_browse," +
                        "i.show_in_home) " +
                        "FROM ImagesDB i " +
                        "WHERE i.id = :reqId", ImageDescription.class)
                .setParameter("reqId", id)
                .getSingleResult();
        if (!auth.isAuthenticated() && !imageDescription.isIspublic()) {
            throw new ForbiddenException("Requesting a non public image while not authenticated is forbidden.");
        }
        return imageDescription;
    }

    @Transactional
    public ImageDescriptionUpdateStatus updateImageMeta(Long id, String camera, String title, String author,
                                                        String datetime, String description, String tags,
                                                        boolean ispublic, boolean showInBrowse, boolean showInHome) {
        auth.ensureIsAdmin();
        log.info("[description update] " + id.toString() + "  " + camera +
                "  " + title + "  " + author + "  " + datetime + "  " + description + "  " + tags + " public=" + ispublic
                + " show_in_browse=" + showInBrowse + " show_in_home=" + showInHome);
        ImagesDB img = entityManager.find(ImagesDB.class, id);
        img.setCamera(camera);
        img.setAuthor(author);
        img.setDatetime(datetime);
        img.setDescription(description);
        img.setTitle(title);
        img.setTags(tags);
        img.setIspublic(ispublic);
        img.setShow_in_browse(showInBrowse);
        img.setShow_in_home(showInHome);
        return new ImageDescriptionUpdateStatus("success");
    }

    @Transactional
    public ImageDescriptionUpdateStatus rotateImage(Long id, int rotationDeg, List<Integer> qualities) {
        auth.ensureIsAdmin();
        try {
            ImagesDB img = this.findById(id);
            if (qualities.contains(1)) {
                BufferedImage original_image_small = ImageIO.read(new ByteArrayInputStream(img.getImage_small()));
                ByteArrayOutputStream baosImageSmall = new ByteArrayOutputStream();
                BufferedImage rotatedThumb = ImageProcessor.rotateImage(original_image_small, rotationDeg);
                ImageIO.write(rotatedThumb, "jpg", baosImageSmall);
                byte[] newImageSmall = baosImageSmall.toByteArray();
                img.setImage_small(newImageSmall);
            }

            if (qualities.contains(2)) {
                BufferedImage original_thumb = ImageIO.read(new ByteArrayInputStream(img.getThumb()));
                ByteArrayOutputStream baosThumb = new ByteArrayOutputStream();
                BufferedImage rotatedThumb = ImageProcessor.rotateImage(original_thumb, rotationDeg);
                ImageIO.write(rotatedThumb, "jpg", baosThumb);
                byte[] newThumbnail = baosThumb.toByteArray();
                img.setThumb(newThumbnail);
            }

            if (qualities.contains(3)) {
                BufferedImage original_minithumb = ImageIO.read(new ByteArrayInputStream(img.getMini_thumb()));
                ByteArrayOutputStream baosminiThumb = new ByteArrayOutputStream();
                BufferedImage rotatedMiniThumb = ImageProcessor.rotateImage(original_minithumb, rotationDeg);
                ImageIO.write(rotatedMiniThumb, "jpg", baosminiThumb);
                byte[] newMiniThumb = baosminiThumb.toByteArray();
                img.setMini_thumb(newMiniThumb);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApiException(Response.Status.INTERNAL_SERVER_ERROR, "Failed to load and rotate image.");
        }
        log.info("Successfully rotated images of quality " +
                qualities + " for id #" + id + " by " + rotationDeg + "°.");
        return new ImageDescriptionUpdateStatus("success");
    }

    @Transactional
    public void tagMapToImages(List<Long> newImageIDs, List<Long> oldImageIDs, Long mapID) {
        try {
            newImageIDs.stream()
                    .filter(imgID -> !oldImageIDs.contains(imgID))
                    .filter(imgID -> findById(imgID) != null)
                    .forEach(imgID -> {
                        log.info("Updating image " + imgID + ": add map reference " + mapID);
                        ImagesDB image = findById(imgID);
                        image.setMap_appearance(mapID);
                    });

            oldImageIDs.stream()
                    .filter(imgID -> !newImageIDs.contains(imgID))
                    .forEach(imgID -> {
                        log.info("Updating image " + imgID + ": remove map reference " + mapID);
                        ImagesDB image = findById(imgID);
                        if (Objects.equals(image.getMap_appearance(), mapID)) {
                            image.setMap_appearance(null);
                        }
                    });
        } catch (Exception e) {
            throw new ApiException(Response.Status.INTERNAL_SERVER_ERROR, "Error while tagging map to images");
        }
    }

    @Transactional
    public void tagCollectionToImages(List<Long> newImageContent, List<List<Long>> newCarouselContent,
                                      CollectionDB collection) {
        // very inefficient
        Long oldCollectionID = collection.getId();
        newImageContent.stream()
                .filter(imgID -> !collection.getImageContent().contains(imgID))
                .forEach(imgID -> {
                    log.info("Updating image " + imgID + ": add collection " + oldCollectionID);
                    ImagesDB image = findById(imgID);
                    List<Long> collectionAppearances = image.getCollections_appearance();
                    if (!collectionAppearances.contains(oldCollectionID)) {
                        collectionAppearances.add(oldCollectionID);
                    }
                    image.setCollections_appearance(collectionAppearances);
                });

        collection.getImageContent().stream()
                .filter(imgID -> !newImageContent.contains(imgID))
                .forEach(imgID -> {
                    log.info("Updating image " + imgID + ": remove collection " + oldCollectionID);
                    ImagesDB image = findById(imgID);
                    List<Long> collectionAppearances = image.getCollections_appearance();
                    collectionAppearances.remove(oldCollectionID);
                    image.setCollections_appearance(collectionAppearances);
                });

        newCarouselContent.forEach(list -> list.stream()
                .filter(imgID -> collection.getCarouselContent().stream().noneMatch(longs -> longs.contains(imgID)))
                .forEach(imgID -> {
                    log.info("Updating image " + imgID + ": add collection " + oldCollectionID);
                    ImagesDB image = findById(imgID);
                    List<Long> collectionAppearances = image.getCollections_appearance();
                    if (!collectionAppearances.contains(oldCollectionID)) {
                        collectionAppearances.add(oldCollectionID);
                    }
                    image.setCollections_appearance(collectionAppearances);
                }));

        collection.getCarouselContent().forEach(list -> list.stream()
                .filter(imgID -> newCarouselContent.stream().noneMatch(longs -> longs.contains(imgID)))
                .forEach(imgID -> {
                    log.info("Updating image " + imgID + ": remove collection " + oldCollectionID);
                    ImagesDB image = findById(imgID);
                    List<Long> collectionAppearances = image.getCollections_appearance();
                    collectionAppearances.remove(oldCollectionID);
                    image.setCollections_appearance(collectionAppearances);
                }));
    }
}
