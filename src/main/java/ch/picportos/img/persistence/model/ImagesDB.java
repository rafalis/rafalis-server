package ch.picportos.img.persistence.model;

import ch.picportos.common.converters.LongListConverter;
import ch.picportos.common.converters.StringListConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "IMAGES")
public class ImagesDB {
    @GeneratedValue
    @Id
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String title;

    private boolean show_in_browse = true;

    private boolean ispublic = false;

    private boolean show_in_home = false;

    private String description;

    private String datetime;

    private String author;

    private String camera;

    private String tags; // separated by ,

    @NotBlank()
    private byte[] image_small;

    @NotBlank()
    private byte[] thumb;

    @NotBlank()
    private byte[] image;

    @NotBlank()
    private byte[] mini_thumb;

    @Convert(converter = StringListConverter.class)
    private List<String> palette;

    @Convert(converter = LongListConverter.class)
    private List<Long> collections_appearance;

    private Long map_appearance;

    private Integer small_width;
    private Integer small_height;
    private Integer thumb_width;
    private Integer thumb_height;
    private Integer minithumb_width;
    private Integer minithumb_height;
}
